#ifndef _INPUT_FORMATS_H_
#define _INPUT_FORMATS_H_

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "image_file.h"

enum bmp_read_status {
    BMP_READ_OK,
    BMP_READ_BAD_HANDLER,
    BMP_READ_BAD_PIXELS,
    BMP_READ_ERR
};

enum bmp_write_status {
    BMP_WRITE_OK,
    BMP_WRITE_HEADER_ERR,
    BMP_WRITE_PIXELS_ERR,
    BMP_WRITE_ERR
};

struct __attribute__((packed)) bmp_header
{
    uint16_t bf_type;
    uint32_t bf_file_size;
    uint32_t bf_reserved;
    uint32_t bf_off_bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_x_pels_per_meter;
    uint32_t bi_y_pels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;
};

enum bmp_read_status read_bmp(struct image_file* image_file, struct image* image);
enum bmp_write_status write_bmp(struct image_file* image_file, struct image* image);

#endif
