section .data
; sepia transformation matrix (b)(g)(r)(b)
blue_vector: dd 0.131, 0.168, 0.189, 0.131
green_vector: dd 0.543, 0.686, 0.769, 0.543
red_vector: dd 0.272, 0.349, 0.393, 0.272

; max value for each channel is 255
max_pixel_values_vector: dd 0xFF, 0xFF, 0xFF, 0xFF

%macro clear_regs 0
    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2
%endmacro

%macro load_sepia_matrix 0
    movaps xmm3, [blue_vector]
    movaps xmm4, [green_vector]
    movaps xmm5, [red_vector]
%endmacro

; (b)(r)(g)(b) --> (g)(b)(r)(g) --> (r)(g)(b)(r)
%define sepia_matrix_shuffle_pattern 0b01001001
%macro shuffle_sepia_matrix 0
    shufps xmm3, xmm3, sepia_matrix_shuffle_pattern
    shufps xmm4, xmm4, sepia_matrix_shuffle_pattern
    shufps xmm5, xmm5, sepia_matrix_shuffle_pattern
%endmacro

; we need different shuffle for each batch because:
; initial state:
; xmm0  --  b[i+1] 0 0 b[i]
; xmm1  --  g[i+1] 0 0 g[i]
; xmm2  --  r[i+1] 0 0 r[i]
; for frist batch:
; xmm0  --  b[1] b[0] b[0] b[0]
; xmm1  --  g[1] g[0] g[0] g[0]
; xmm2  --  r[1] r[0] r[0] r[0]
; for second batch:
; xmm0  --  b[2] b[2] b[1] b[1]
; xmm1  --  g[2] g[2] g[1] g[1]
; xmm2  --  r[2] r[2] r[1] r[1]
; for thrid batch:
; xmm0  --  b[3] b[3] b[3] b[2]
; xmm1  --  g[3] g[3] g[3] g[2]
; xmm2  --  r[3] r[3] r[3] r[2]
;
; %1 -- batch number
%macro load_batch 1
    pinsrb xmm0, [rdi], 0
    pinsrb xmm0, [rdi+3], 12
    pinsrb xmm1, [rdi+1], 0 
    pinsrb xmm1, [rdi+4], 12
    pinsrb xmm2, [rdi+2], 0
    pinsrb xmm2, [rdi+5], 12
    
    %if %1 == 1
        %define shuffle_pattern 0b11000000
    %elif %1 == 2
        %define shuffle_pattern 0b11110000
    %else
        %define shuffle_pattern 0b11111100
    %endif
    
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2

    shufps xmm0, xmm0, shuffle_pattern
    shufps xmm1, xmm1, shuffle_pattern
    shufps xmm2, xmm2, shuffle_pattern
%endmacro

; just multiply and sum components of vectors
%macro perform_sepia 0
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2
%endmacro

; write batch to memory
; xmm0 -- b[1] r[0] g[0] b[0]  or  g[2] b[2] r[1] g[1]  or  r[3] g[3] b[3] r[2]
%macro save_batch 0
    cvtps2dq xmm0, xmm0
    pminsd xmm0, [max_pixel_values_vector]
    pextrb [rsi], xmm0, 0
    pextrb [rsi+1], xmm0, 4
    pextrb [rsi+2], xmm0, 8
    pextrb [rsi+3], xmm0, 12
%endmacro

; batchs iteration
; %1 -- iterations count
%assign batch_number 1
%macro iterate 1
    %rep %1
        %assign n batch_number
        clear_regs
        load_batch n
        perform_sepia
        save_batch

        add rsi, 4
        add rdi, 3
        shuffle_sepia_matrix
        %assign batch_number n+1
    %endrep
%endmacro


section .text
global sepia_batch_asm
;; 4 pixels
;; rdi -- pixels start
;; rsi -- new pixels start
;; (rsi/rdi) -> [b1][g1][r1][b2][g2][r2][b3][g3][r3][b4][g4][r4]  (size = 12bytes)
sepia_batch_asm:
    load_sepia_matrix

    iterate 3

    ret
