#include "transformations.h"

#include "error_handlers.h"

void rotate_90_counterclockwise(struct image* image, struct image* new_image) {
    if (new_image->pixels) free(new_image->pixels);
    struct pixel* new_pixels = allocate_pixels_memory(image->width, image->height);
    handle_allocate_pixels_memory_error(new_pixels);
    if (new_pixels) {
        for (size_t i=0; i<image->height; i++) {
            for (size_t j=0; j<image->width; j++) {
                new_pixels[(j * image->height) + (image->height - i - 1)] = (image->pixels)[(i * image->width) + j];
            }
        }
        new_image->width = image->height;
        new_image->height = image->width;
        new_image->pixels = new_pixels;
    }
}


const float transformation_matrix[3][3] = {
        {0.393f, 0.769f, 0.189f},
        {0.349f, 0.686f, 0.168f},
        {0.272f, 0.543f, 0.131f},
};

uint8_t trim(double val) {
    if (val > 255.0) return 255;
    return val;
}

void sepia_pixel_c(struct pixel *pixel, struct pixel *new_pixel) {
    new_pixel->r = trim(pixel->r * transformation_matrix[0][0] + pixel->g * transformation_matrix[0][1] + pixel->b * transformation_matrix[0][2]);
    new_pixel->g = trim(pixel->r * transformation_matrix[1][0] + pixel->g * transformation_matrix[1][1] + pixel->b * transformation_matrix[1][2]);
    new_pixel->b = trim(pixel->r * transformation_matrix[2][0] + pixel->g * transformation_matrix[2][1] + pixel->b * transformation_matrix[2][2]);
}


void sepia_c(struct image *image, struct image *new_image) {
    new_image->width = image->width;
    new_image->height = image->height;
    for (size_t i=0; i<image->width*image->height; i++) {
        sepia_pixel_c(image->pixels+i, new_image->pixels+i);
    }
}

extern void sepia_batch_asm(struct pixel *pixel, struct pixel *new_pixel);
void sepia_asm(struct image *image, struct image *new_image) {
    new_image->width = image->width;
    new_image->height = image->height;
    int batch_count = image->width*image->height / BATCH_SIZE;
    for (size_t i=0; i<batch_count; i++) {
        sepia_batch_asm(image->pixels+i*BATCH_SIZE, new_image->pixels+i*BATCH_SIZE);
    }

    for (size_t i=batch_count*BATCH_SIZE+BATCH_SIZE; i<image->width*image->height; i++) {
        sepia_pixel_c(image->pixels+i, new_image->pixels+i);
    }
}