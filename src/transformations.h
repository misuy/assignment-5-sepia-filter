#ifndef _TRANSFORMATIONS_H_
#define _TRANSFORMATIONS_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

#include "image.h"

#define BATCH_SIZE 4

void rotate_90_counterclockwise(struct image* image, struct image* new_image);
void sepia_c(struct image *image, struct image *new_image);
void sepia_asm(struct image *image, struct image *new_image);

#endif
