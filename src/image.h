#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* pixels;
};

struct image create_image(void);

struct pixel* allocate_pixels_memory(uint64_t width, uint64_t height);

void free_image_memory(struct image* image);

bool are_images_equal(struct image *image1, struct image *image2);

#endif
