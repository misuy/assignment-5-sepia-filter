#ifndef _TEST_H_
#define _TEST_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "error_handlers.h"
#include "image.h"
#include "image_file.h"
#include "input_formats.h"
#include "transformations.h"

void perform_tests(int test_count, char** test_paths);

#endif