#include "test.h"


#define myprintf(fd, format, ...) {   \
    printf(format, __VA_ARGS__);      \
    fprintf(fd, format, __VA_ARGS__); \
}


void perform_test(char* test_dir_path, int test_number) {
    char test_log_path[256] = "\0";
    strcat(test_log_path, test_dir_path);
    strcat(test_log_path, "/test_log.txt");
    FILE* test_log_file = fopen(test_log_path, "w");

    printf("----------------------\n");
    myprintf(test_log_file, "test %d is running...\n", test_number);
    myprintf(test_log_file, "test directory: %s\n", test_dir_path);

    char input_path[256] = "\0";
    strcat(input_path, test_dir_path);
    strcat(input_path, "/input.bmp");

    char asm_output_path[256] = "\0";
    strcat(asm_output_path, test_dir_path);
    strcat(asm_output_path, "/output_asm.bmp");

    char c_output_path[256] = "\0";
    strcat(c_output_path, test_dir_path);
    strcat(c_output_path, "/output_c.bmp");

    struct maybe_image_file maybe_input_file = open_image_file(input_path, "rb");
    handle_open_image_file_error(&maybe_input_file);
    struct image_file input_file = maybe_input_file.image_file;
    struct image image = create_image();
    handle_read_bmp_error(&input_file, read_bmp(&input_file, &image));

    struct image sepia_performed_image_c = create_image();
    sepia_performed_image_c.pixels = allocate_pixels_memory(image.width, image.height);
    struct image sepia_performed_image_asm = create_image();
    sepia_performed_image_asm.pixels = allocate_pixels_memory(image.width, image.height);

    struct rusage r;


    getrusage(RUSAGE_SELF, &r);
    struct timeval c_start = r.ru_utime;

    sepia_c(&image, &sepia_performed_image_c);

    getrusage(RUSAGE_SELF, &r);
    struct timeval c_end = r.ru_utime;
    uint64_t c_time = (c_end.tv_sec - c_start.tv_sec) * 1000000L + c_end.tv_usec - c_start.tv_usec;


    getrusage(RUSAGE_SELF, &r);
    struct timeval asm_start = r.ru_utime;

    sepia_asm(&image, &sepia_performed_image_asm);

    getrusage(RUSAGE_SELF, &r);
    struct timeval asm_end = r.ru_utime;
    uint64_t asm_time = (asm_end.tv_sec - asm_start.tv_sec) * 1000000L + asm_end.tv_usec - asm_start.tv_usec;

    
    myprintf(test_log_file, "asm sepia implementation execution time: %" PRIu64 " us;\n", asm_time);
    myprintf(test_log_file, "c sepia implementation execution time: %" PRIu64 " us;\n", c_time);
    if (asm_time < c_time) {
        myprintf(test_log_file, "asm implementation faster by %" PRIu64 " us!\n", c_time - asm_time);
    }
    else {
        myprintf(test_log_file, "asm implementation slower by %" PRIu64 " us!\n", asm_time - c_time);
    }


    struct maybe_image_file maybe_output_file_asm = open_image_file(asm_output_path, "wb");
    handle_open_image_file_error(&maybe_output_file_asm);
    struct image_file output_file_asm = maybe_output_file_asm.image_file;
    handle_write_bmp_error(&output_file_asm, write_bmp(&output_file_asm, &sepia_performed_image_asm));

    struct maybe_image_file maybe_output_file_c = open_image_file(c_output_path, "wb");
    handle_open_image_file_error(&maybe_output_file_c);
    struct image_file output_file_c = maybe_output_file_c.image_file;
    handle_write_bmp_error(&output_file_c, write_bmp(&output_file_c, &sepia_performed_image_c));

    free_image_memory(&image);
    free_image_memory(&sepia_performed_image_c);
    free_image_memory(&sepia_performed_image_asm);

    handle_close_image_file_error(&input_file, close_image_file(&input_file));
    handle_close_image_file_error(&output_file_asm, close_image_file(&output_file_asm));
    handle_close_image_file_error(&output_file_c, close_image_file(&output_file_c));

    myprintf(test_log_file, "test %d is finished.\n", test_number);
    fclose(test_log_file);

    printf("----------------------\n\n");
}



void perform_tests(int test_count, char** test_paths) {
    for (int i=1; i<test_count; i++) {
        perform_test(test_paths[i], i);
    }
}
