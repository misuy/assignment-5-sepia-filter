#include "image.h"

struct image create_image(void) {
    struct image image = {.width = 0, .height = 0, .pixels = NULL};
    return image;
}

struct pixel* allocate_pixels_memory(uint64_t width, uint64_t height) {
    struct pixel* pixels = malloc(sizeof(struct pixel) * width * height);
    return pixels;
}

void free_image_memory(struct image* image) {
    free(image->pixels);
}


bool are_pixels_equal(struct pixel *pixel1, struct pixel *pixel2) {
    return (pixel1->r == pixel2->r) && (pixel1->g == pixel2->g) && (pixel1->b == pixel2->b);
}


bool are_images_equal(struct image *image1, struct image *image2) {
    if ((image1->width != image2->width) || (image1->height != image2->height)) return false;
    for (int i=0; i<image1->width*image1->height; i++) {
        if (!are_pixels_equal(image1->pixels+i, image2->pixels+i)) return false;
    }
    return true;
}
