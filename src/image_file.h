#ifndef _IMAGE_FILE_H_
#define _IMAGE_FILE_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

enum image_file_open_status {
    IMAGE_FILE_OPEN_OK,
    IMAGE_FILE_OPEN_BAD_PATH,
    IMAGE_FILE_OPEN_ACCESS_DENIED,
    IMAGE_FILE_OPEN_SOMETHING_WENT_WRONG
};

struct image_file {
    char* path;
    FILE* file;
};

struct maybe_image_file {
    struct image_file image_file;
    enum image_file_open_status open_status;
};


struct maybe_image_file open_image_file(char* path, const char* mode);

int close_image_file(struct image_file* image_file);

int image_file_read(void* buffer, size_t size, size_t count, struct image_file* image_file);

int image_file_write(void* buffer, size_t size, size_t count, struct image_file* image_file);

int image_file_seek(struct image_file* image_file, uint32_t offset, int seek_pos);

#endif
