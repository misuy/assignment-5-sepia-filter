CFLAGS     = -g -Wall -Werror -std=c17 -Wno-unused-function -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion -fno-plt
ASMFLAGS   = -g -felf64
CC         = gcc
LD         = gcc
ASM        = nasm
SRC_DIR    = src
BUILD_DIR  = build
TARGET     = assignment-5


all: $(TARGET)

$(TARGET): $(BUILD_DIR)/main.o $(BUILD_DIR)/sepia_batch_asm.o $(BUILD_DIR)/error_handlers.o $(BUILD_DIR)/image.o $(BUILD_DIR)/image_file.o $(BUILD_DIR)/input_formats.o $(BUILD_DIR)/transformations.o $(BUILD_DIR)/test.o
	$(LD) -no-pie -o $@ $^

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	$(RM) $(TARGET) $(BUILD_DIR)/*.o

.PHONY: clean all

